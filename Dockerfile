FROM node:alpine

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN git clone https://github.com/smart-on-fhir/bp-centiles-app.git
WORKDIR /bp-centiles-app
RUN npm install
ADD /images/logo.png /bp-centiles-app/images/logo.png
ADD /images/hspc-company-logo.png /bp-centiles-app/images/hspc-company-logo.png
ADD /src/.well-known/smart/manifest*.json /bp-centiles-app/.well-known/smart/
RUN mv /bp-centiles-app/index.html /bp-centiles-app/app.html
ADD /src/index.html /bp-centiles-app/index.html
ADD /src/css/app.css /bp-centiles-app/css/app.css
RUN rm /bp-centiles-app/launch.html
ADD /src/launch.html /bp-centiles-app/launch.html
ARG TARGET_ENV
ENV TARGET_ENV=$TARGET_ENV
RUN if [ "$TARGET_ENV" = "prod" ]; then rm /bp-centiles-app/.well-known/smart/manifest.json; mv /bp-centiles-app/.well-known/smart/manifest.prod.json /bp-centiles-app/.well-known/smart/manifest.json; elif [ "$TARGET_ENV" = "test" ]; then rm /bp-centiles-app/.well-known/smart/manifest.json; mv /bp-centiles-app/.well-known/smart/manifest.test.json /bp-centiles-app/.well-known/smart/manifest.json; fi
CMD ["./node_modules/http-server/bin/http-server", "-p", "8000", "-c-1", ".", "--cors"]