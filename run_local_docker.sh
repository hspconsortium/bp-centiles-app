#!/usr/bin/env bash

docker build --build-arg ACTIVE_ENV=local -t bp-centiles-app/node-web-app .
docker run -p 8000:8000 bp-centiles-app/node-web-app